Pod::Spec.new do |s|
  s.name = "AdobeCreativeSDKCore"
  s.version = "0.0.1"
  s.summary = "Adobe CreativeSDK for iOS Image Dynamic Framework."
  s.description = "Adobe CreativeSDK Core"
  s.homepage = "https://www.example.com/"
  s.license = {
    :type => "Commercial"
  }
  s.authors = {
    "SoulInCode" => "hello@sic.studio"
  }
  s.documentation_url = "https://creativesdk.adobe.com/docs/ios/#/index.html"
  s.source = {
    "git": "git@bitbucket.org:krush42/adobecreativesdk.git"
  }
  s.platform = :ios, "8.0"
  s.libraries =  "z", "sqlite3"
  s.requires_arc = true
  s.source_files = "AdobeCoreLib/AdobeCreativeSDKCore.framework/Versions/A/Headers/AdobeCreativeSDKCore.h"
  s.resources = "AdobeCoreLib/AdobeCreativeSDKCore.framework/Versions/A/Resources/AdobeCreativeSDKCoreResources.bundle"
  s.frameworks = "AdobeCreativeSDKCore", "UIKit", "WebKit", "OpenGLES", "QuartzCore", "StoreKit", "SystemConfiguration", "MessageUI", "CoreData", "Foundation"
  s.preserve_paths = "*"
  s.xcconfig = { "FRAMEWORK_SEARCH_PATHS" => "$(PODS_ROOT)/AdobeCreativeSDKCore/AdobeCoreLib/" }
end
